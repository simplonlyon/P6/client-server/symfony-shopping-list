<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\ProductList;
use App\Entity\Product;
use App\Repository\ProductListRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;

class ShoppingListController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/shopping-list", name="all_shopping_list", methods={"GET"})
     */
    public function all(ProductListRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/shopping-list/{productList}", name="one_shopping_list", methods={"GET"})
     */
    public function one(ProductList $productList)
    {
        $data = $this->serializer->normalize($productList, null, ['attributes' => ['id', 'name', 'products' => ['id', 'name', 'comment', 'tag', 'qty', 'done']]]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/shopping-list", name="new_shopping_list", methods={"POST"})
     */
    public function add(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $productList = $this->serializer->deserialize($content, ProductList::class, "json");

        $manager->persist($productList);
        $manager->flush();

        $data = $this->serializer->normalize($productList, null, ['attributes' =>['id', 'name']]);

        $response = new Response($this->serializer->serialize($productList, "json"));
        return $response;
    }

    /**
     * @Route("/shopping-list/{productList}/product", name="new_product", methods={"POST"})
     */
    public function addProduct(Request $request, ProductList $productList)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $product = $this->serializer->deserialize($content, Product::class, "json");

        $productList->addProduct($product);

        $manager->persist($productList);
        $manager->flush();

        $data = $this->serializer->normalize($product, null, ['attributes' => ['id', 'name', 'comment', 'tag', 'qty', 'done']]);        

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/shopping-list/{productList}", name="delete_shopping_list", methods={"DELETE"})
     */
    public function del(ProductList $productList)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($productList);
        $manager->flush();

        return new Response("OK", 204);
    }

    /**
     * @Route("/shopping-list/{productList}", name="updatde_shopping_list", methods={"PUT"})
     */
    public function upd(Request $request, ProductList $productList)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, ProductList::class, "json");

        $productList->setName($update->getName());

        $manager->persist($productList);
        $manager->flush();

        $data = $this->serializer->normalize($product, null, ['attributes' => ['id', 'name']]);        

        $response = new Response($this->serializer->serialize($productList, "json"));
        return $response;
    }
}
